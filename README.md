# Aula 9 - Disciplina Introdução a BigData. 
## Objetivo
Praticar os conceitos de manipulação de arquivos CSV e compilar um novo CSV

## Desafio! Preparar dados

Nessa aula iremos executar um código em Python e realizar pequenas modificações.

## Nosso problema:

* Um produtor de soja e precisa saber o melhor momento para vender o meu produto?

* As varáveis mais significativos para exportação são o preço do Dolar e Cotação da commodity no exterior.



## Perguntas:

- Como organizar dados de catações de Dolar e do produto Soja?

- Quais dados nos arquivos podem ser limpos? 

- Você consegui imaginar outras aplicações para o conteúdo aprendido?

## Ambiente de Desenvolvimento

Ser possível executar um código em Python.
Exemplo: `python lerCSV.py` no Terminal.

a) Usar a IDE Geany [LINK] (https://www.geany.org/)

b) Usar o Python [LINK] (https://www.python.org/)

c) Recomendável utilizar a IDE - VisualStudio Code em ambiente Linux.

## Recursos

Computadores.

### Como preparar o ambiente com Python

Instalando o Python em diferentes ambientes [LINK] (http://www.programeempython.com.br/blog/instalando-o-python/)


### Como clonar o Projeto

`git clone https://gitlab.com/nallaworks/Ibigdata_Aula9`

## Entrega
Pessoal a entrega será: 

Entrega!
[bit.ly/bdag1] (http://bit.ly/bdag1)
* Usando os arquivos fornecidos em aula com dados de soja e dolar, gerar um código que salve um alquivo com o nome dados_novos.csv contendo a seguinte estrutura.
 - ’Data’ , ’Cultura’ , ’cotação’ , ’Valor de venda do dolar ’


### Enviar para o email:

* allan@fatecpompeia.edu.br
* Assunto ”[IntBigData] – Dados de soja e dolar”.
* Até dia 25/09`
* Outros dados:

* Prof: 
* Whats: (14) 98199-7228


Grato, 
Allan
